/**
 * authors:     Max Braungardt & Hannes Thalheim
 */

#include "HC_SR04.h"

HC_SR04::HC_SR04( int puls, int echo, int idleState ) {

  this->puls = puls;
  this->echo = echo;
  this->idleState = idleState;

  pinMode( this->puls, OUTPUT );
  pinMode( this->echo, INPUT );
  digitalWrite( this->puls, this->idleState );
};

double HC_SR04::getDistance( double temperature, unsigned long timeout ) {

  pulse( puls, pulsDuration );
  return distance( echoDuration(echo, timeout), sonicspeed(temperature) );
};

void HC_SR04::pulse( int pin, int duration ) {

  digitalWrite( pin, !idleState );
  delay( duration );
  digitalWrite( pin, idleState );
  return;
};

unsigned long HC_SR04::echoDuration( int pin, unsigned long timeout ) {

  // Abort if initial state is wrong
  if (digitalRead( pin ) != idleState) {

    return 0;
  } else {

    unsigned long now = micros();
    unsigned long start = 0;

    while (digitalRead( pin ) == idleState) {

      // Wait or abort:
      if ((micros() - now) >= timeout) { return 0; }
    }

    start = micros();
    while (digitalRead(pin) != idleState) {

      // Wait or abort:
      if ((micros() - now) >= timeout) { return 0; }
    }

    return micros() - start;
  }
};

double HC_SR04::sonicspeed( double temperature ) {

  return 331.5 + 0.6 * temperature;
};

double HC_SR04::distance( double duration, double sonicspeed ) {

  double distance = duration * sonicspeed / 2;
  return (3000.0 <= distance && distance <= 3000000.0) ? distance : 0;
};
