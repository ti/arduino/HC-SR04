/**
 * authors:     Max Braungardt & Hannes Thalheim
 */

#ifndef HC_SR04__HPP
#define HC_SR04__HPP

#include <Arduino.h>

class HC_SR04 {

  public:
	/**
	 * Constructor
	 */
	HC_SR04( int puls, int echo, int idleState = LOW );

	/**
	 * Sends out a pulse and measures the time of its echo in µs.
	 * Returns the distance in µm or just 0 if the result is out
	 * the range of the sensor-specs (there was a measure-error).
	 *
	 * It is recommended to rely only on distances
	 * between 3cm and 2m.
	 */
	double getDistance( double temperature = 20.0, unsigned long timeout = 500000 );

  private:
	int puls;
	int echo;
	int idleState;
	const unsigned long pulsDuration = 10;

	void pulse( int pin, int duration );

	/**
	 * Measures and returns the duration of echo of a pulse in µs.
	 * Returns 0 after a given timeout-time is over
	 * and no complete echo was measured correctly.
	 */
	unsigned long echoDuration( int pin, unsigned long timeout );

	/**
	 * Calculates and returns the current sonicspeed
	 * depending on the given temperature.
	 */
	double sonicspeed( double temperature );

	/**
	 * Calculates and returns the distance in µm from
	 * given echo-duration and sonicspeed.
	 * Returns 0 if the result is out the range of the sensor-specs.
	 */
	double distance( double duration, double sonicspeed );
};

#endif
