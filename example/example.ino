/**
 * authors:     Max Braungardt & Hannes Thalheim
 */

#include <DHT11.h>
#include <HC_SR04.h>

DHT11 thermometer = DHT11( 5 );
HC_SR04 sonic = HC_SR04( 2, 3 );

void setup() {

  Serial.begin( 9600 );
  thermometer.readSensor();
  Serial.println( String(thermometer.getTemperature(), 1) + " °C" );
}

void loop() {

  double distance = sonic.getDistance( thermometer.getTemperature() );
  if (distance) {
    Serial.println( String(distance/1000000.0, 3) + " m" );
  } else {
    Serial.println( "OUT OF RANGE" );
  }

  delay( 500 );
}

